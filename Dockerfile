FROM ubuntu AS build

ARG DEBIAN_FRONTEND=noninteractive

ADD ./3rdparty /app/3rdparty
WORKDIR /app/3rdparty/ngspice

RUN apt-get update \
    && apt-get install apt-transport-https -y \
    && apt-get install gcc -y \
    && apt-get install g++ -y \
    && apt-get install cmake -y \
    && apt-get install wget -y \
    && apt-get install build-essential -y \
    && apt-get install libgtk2.0-dev -y \
    && apt-get install libxaw7-dev -y \
    && apt-get install flex -y \
    && apt-get install bison -y

RUN ./autogen.sh
 
WORKDIR /app/3rdparty/ngspice/build

RUN ../configure --with-ngshared --enable-xspice --disable-debug --enable-cider --enable-openmp
RUN make 2>&1 | tee make.log
RUN make install

RUN apt-get install libboost-all-dev -y

ADD ./src /app/src
WORKDIR /app/build

RUN cmake ../src && \
    cmake --build .

ENTRYPOINT ["/app/src/bin/asynchronous_multithreaded_server", "127.0.0.1", "5000", "1", "/app/src/sources"]

EXPOSE 5000
