#ifndef ASYNCHRONOUS_MULTITHREADED_SERVER_CONNECTION_H
#define ASYNCHRONOUS_MULTITHREADED_SERVER_CONNECTION_H

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/io_context.hpp>
#include "reply.h"

class Connection: public boost::enable_shared_from_this<Connection>, private boost::noncopyable {
public:
    explicit Connection(boost::asio::io_context& io_context);
    boost::asio::ip::tcp::socket& socket();
    void start();

private:
    void handle_read(const boost::system::error_code& e, std::size_t bytes_transferred);
    void handle_write(const boost::system::error_code& e);
    boost::asio::io_context::strand strand_;
    boost::asio::ip::tcp::socket socket_;
    boost::array<char, 8192> buffer_;
    std::string request_;
    Reply reply_;
};

typedef boost::shared_ptr<Connection> connection_ptr;

#endif //ASYNCHRONOUS_MULTITHREADED_SERVER_CONNECTION_H
