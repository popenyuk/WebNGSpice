#ifndef ASYNCHRONOUS_MULTITHREADED_SERVER_REPLY_H
#define ASYNCHRONOUS_MULTITHREADED_SERVER_REPLY_H

#include <string>
#include <vector>
#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>

struct Reply {
    enum status_type {
        ok = 200,
        internal_server_error = 500,
        not_found = 404
    } status;

    std::string content;
    std::string headers;

    std::vector<boost::asio::const_buffer> to_buffers();
    static Reply stock_reply(status_type status);
};

struct status_strings {
    static boost::asio::const_buffer to_buffer(Reply::status_type status);

    static const std::string ok;
    static const std::string internal_server_error;
    static const std::string not_found;
};

struct stock_replies {
    static const char ok[];
    static const char internal_server_error[];
    static const char not_found[];

    static std::string to_string(Reply::status_type status) {
        switch (status) {
            case Reply::ok:
                return ok;
            case Reply::not_found:
                return not_found;
            default:
                return internal_server_error;
        }
    }
};

#endif //ASYNCHRONOUS_MULTITHREADED_SERVER_REPLY_H
